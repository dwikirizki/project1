<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Anggota extends Model
{
// datainputan user
    protected $table = 'table_anggota';
    protected $fillabel = ['nama_anggota','jenis_kelamin','alamat','email','no_tlp'];
}
